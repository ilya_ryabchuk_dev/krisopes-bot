'use strict';

(function () {

    var Botkit  = require('botkit');
    var config  = require('./config');
    var env     = require('../local.env');

    var request = require('request');

    var askPattern = [
        {
            pattern: 'да',
            callback: function (response, convo) {
                convo.next();
            }
        },

        {
            pattern: 'нет',
            callback: function (response, convo) {
                convo.stop();
            }
        },

        {
            default: true,
            callback: function (response, convo) {
                convo.repeat();
                convo.next();
            }
        }
    ];

    var controller = Botkit.slackbot({
       debug: false
    });

    var bot = controller.spawn({
        token: env.SLACK_BOT_TOKEN
    });

    bot.startRTM();

    controller.hears(config.HELLO_KEYWORDS, config.mentionOptions.AMBIENT, function (bot, message) {
        bot.reply(message, config.BOT_MESSAGE);
    });

    controller.hears(config.HELLO_KEYWORDS, config.mentionOptions.DIRECT, function (bot, message) {

        controller.storage.users.get(message.user, function (err, user) {
            if (user && user.name) {
                bot.reply(message, config.HELLO_KEYWORDS[0] + " " + user.name);
            } else {
                startSaveUserConversation(bot, message);
            }
        });

    });

    controller.hears(config.JOKE_KEYWORDS, config.mentionOptions.AMBIENT, function (bot, message) {

        request(config.JOKE_API_URL, onJokeReceived);

        function onJokeReceived(error, response, body) {
            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);

                bot.reply(message, {
                    text: config.JOKE_TITLE,
                    attachments: [
                        {
                            text: data.value,
                            image_url: data.icon_url,
                            thumb_url: data.icon_url,
                            color: '#7CD197'
                        }
                    ]
                });

            } else {
                bot.reply(message, {
                    text: config.JOKE_ERROR,
                    icon_emoji: ":sad:"
                });
            }
        }

    });

    controller.hears(config.TEAM_MENTION, config.mentionOptions.AMBIENT, function (bot, message) {
       bot.reply(message, config.TEAM_MESSAGE);
    });

    controller.hears(config.BOT_NAME, config.mentionOptions.AMBIENT, function (bot, message) {
        bot.reply(message, config.BOT_MESSAGE);
    });

    function startSaveUserConversation(bot, message) {
        bot.startConversation(message, function (err, convo) {
            if (!err) {

                convo.ask(config.ASK_MESSAGE, function (response, convo) {

                    convo.ask(config.ASK_CONFIRM + " " + response.text, askPattern);
                    convo.next();

                }, { 'key': 'nickname' });

                convo.on('end', function (convo) {
                    if (convo.status == 'completed') {

                        controller.storage.users.get(message.user, function (err, user) {
                            if (!user) {
                                user = {
                                    id: message.user
                                };
                            }

                            user.name = convo.extractResponse('nickname');

                            controller.storage.users.save(user, function (err, id) {
                                bot.reply(message, config.ASK_SUCCESS + " " + user.name);
                            });
                        });

                    }
                });
            }
        });
    }

})();
