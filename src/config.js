'use strict';

module.exports = {

	JOKE_KEYWORDS: [
	    'шутку в студию'
    ],

    HELLO_KEYWORDS: [
        'yo',
        'йо',
        'здарова',
    ],

    TEAM_MENTION    : 'крысопсы',
    TEAM_MESSAGE    : 'йо, крысопсы',

    HELLO_MESSAGE   : 'yo\nшо ты там?',

    ASK_MESSAGE     : 'Этот крысопес мне незнаком, как тебя называть ?',
    ASK_SUCCESS     : 'Окей, теперь буду называть тебя',
    ASK_CONFIRM     : 'Ты хочешь чтобы я называл тебя',

    JOKE_API_URL	: 'https://api.chucknorris.io/jokes/random',
    JOKE_ERROR		: 'не зашла шутеечка',
    JOKE_TITLE      : 'Оп, шутеечка подъехала',

    BOT_NAME		:  ['крысопес', 'Крысопес', 'krisopes', 'Krisopes'],
    BOT_MESSAGE	    : 'Чего желаете?',

    mentionOptions : {
	    AMBIENT: 'ambient',
        DIRECT: 'direct_mention, direct_message, mention'
    }

};